document.querySelector('#submit').addEventListener("click", (event) => {
  let phoneInput = document.getElementById('phone');
    let phoneValue = phoneInput.value;

    if (phoneValue.length < 10) {
      alert("Phone number should be 10 characters long.");
      event.preventDefault();
      return false;
    } else { 
      return true;
    }
  });
document.getElementById("myForm").addEventListener("submit", function(event) {
    event.preventDefault(); // Prevent default form submission
    // Gather form data
    var formData = new FormData();
    formData.append("name", document.getElementById("name").value);
    formData.append("email", document.getElementById("email").value);
    formData.append("phone",document.getElementById("phone").value)
    // Create AJAX request
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://89df-103-112-83-12.ngrok-free.app/itdeveloperpractice/api/create.php");
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // Change content type
    xhr.onload = function() {
      if (xhr.status === 200) {
        // Request succeeded
        alert(JSON.parse(xhr.responseText).message);
        myForm.reset();
      } else {
        // Request failed
        console.error(xhr.status);
      }
    };
    xhr.send(new URLSearchParams(formData)); // Convert FormData to URL-encoded format
  });