fetch('https://b1d4-103-112-83-12.ngrok-free.app/itdeveloperpractice/api/read.php')
    .then(function(response) {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error('Error retrieving data.');
      }
    })
    .then(function(data) {
      var dataContainer = document.getElementById('dataContainer');
      var table = document.createElement('table');
      var thead = document.createElement('thead');
      var tbody = document.createElement('tbody');
      // Create table header
      var headerRow = document.createElement('tr');
      var idHeader = document.createElement('th');
      idHeader.textContent = 'ID';
      headerRow.appendChild(idHeader);
      var nameHeader = document.createElement('th');
      nameHeader.textContent = 'Name';
      headerRow.appendChild(nameHeader);
      var emailHeader = document.createElement('th');
      emailHeader.textContent = 'Email';
      headerRow.appendChild(emailHeader);
      var phoneHeader = document.createElement('th');
      phoneHeader.textContent = 'Phone';
      headerRow.appendChild(phoneHeader);
      thead.appendChild(headerRow);
      // Create table rows for data
      if (Array.isArray(data.data)) {
        data.data.forEach(function(item) {
          var row = document.createElement('tr');
          var idCell = document.createElement('td');
          idCell.textContent = item.id;
          row.appendChild(idCell);
          var nameCell = document.createElement('td');
          nameCell.textContent = item.name;
          row.appendChild(nameCell);
          var emailCell = document.createElement('td');
          emailCell.textContent = item.email;
          row.appendChild(emailCell);
          var phoneCell = document.createElement('td');
          phoneCell.textContent = item.phone;
          row.appendChild(phoneCell);
          tbody.appendChild(row);
        });
      } else {
        console.log('Invalid data format:', data);
      }
      // Append table parts to table element
      table.appendChild(thead);
      table.appendChild(tbody);
      // Append table to data container
      dataContainer.appendChild(table);
    })
    .catch(function(error) {
      console.log('Error:', error);
    });


    //----------------------------
    function deleteItem(element, id) {
      mainElement = element;
     let confirmDelete = confirm('Are you sure? this is a destructive command. This will ');
     if(confirmDelete) {
      // Perform the delete request
      fetch('https://b86d-103-112-83-12.ngrok-free.app/php_api/model/delete.php?id=' + id, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          'ngrok-skip-browser-warning': true
        }
      })
        .then(function(response) {
          if (response.ok) {
            $(mainElement).closest('tr').remove(); // Remove the row from the table
          } else {
            throw new Error('Error deleting data.');
          }
        })
        .then(function(data) {
          console.log('Item deleted:', data);
          mainElement = undefined;
        })
        .catch(function(error) {
          console.log('Error:', error);
        });
    } else {
      exit(0); // Exit without deleting anything.
    }
    }