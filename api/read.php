<?php
    //header
    // header('Access-Control-Allow-Origin: *');
    // header('Content-Type: application/json');
    // header('Referrer-Policy: strict-origin-when-cross-origin');
    // header('ngrok-skip-browser-warning: 69420');
    // header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

    // header("Access-Control-Allow-Origin: *"); // Replace "*" with the appropriate origin URL if you want to restrict it
header("Access-Control-Allow-Methods: GET");
// header("Access-Control-Allow-Headers: Content-Type, Authorization");
    //initializing our API
    include_once('../core/initialize.php');

    //instantiate post 
    $post = new Post($db);

    //blog post query
    $result = $post->read();

    //get the row count
    $num = $result->rowCount();

    if($num > 0){
        $post_arr = array();
        $post_arr['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $post_item = array(
                'id' =>$id,
                'title' =>$title,
                'body' => html_entity_decode($body),
                'author' => $author,
                'category_id' => $category_id,
                'category_name' => $category_name
            );
            array_push($post_arr['data'],$post_item);
        }
        //convert to JSON and output
        echo json_encode($post_arr);
    }else{
        echo json_encode(array('message'=>'No posts found.'));
    }
?>
